<?php
/**
 * The template for displaying 404 pages (not found)
 *
 * @link https://codex.wordpress.org/Creating_an_Error_404_Page
 *
 * @package shkenda
 */

get_header();
?>

		<div class="fullBlock leaf-background">
			<div class="form-holder page-404">
				<div class="u-columns col2-set" id="customer_login">
					<div class="u-column1 col-1">
						<div class="woocommerce-form">
							<div class="form-head">
								<h3>404</h3>
								<p class="notFound"><?php the_field('page_not_found', 'option'); ?></p><br>
								<a href="<?php echo home_url(); ?>" class="green-btn returnHome"><?php the_field('return_home', 'option'); ?></a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

<?php
get_footer();
