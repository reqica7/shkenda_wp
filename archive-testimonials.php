<?php
/**
 * The template for displaying archive pages
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package shkenda
 */
get_header();
$paged =(get_query_var('page')) ? get_query_var('page') : 1;
$args = array(  
	'post_type' => 'testimonials',
	'post_status' => 'publish',
	'posts_per_page' => 3, 
	'paged'=>$paged,
	'orderby' => 'ID', 
	'order' => 'DESC', 
);

query_posts($args);
?>

<main id="main" class="main_content">

    <?php if ( have_posts() ) : ?>

    <div class="fullBlock">
        <div class="testimonials testimonialsArchive" id="testimonials-section">
            <div class="container has_gutter">
                <div class="row">
                    <div class="column-12 column-tab-12 column-mob-12 rightSide inf-scroll"
                        data-pagination=".pagination" data-pagination-next=".pagination a"
                        data-item-class=".testimonialsList" data-totalPages="20">

                        <?php
								/* Start the testimonials */
								while ( have_posts() ) :
									the_post();
									
									/*
									* Include the Post-Type-specific template for the content.
									* If you want to override this in a child theme, then include a file
									* called content-___.php (where ___ is the Post Type name) and that will be used instead.
									*/
									get_template_part( 'template-parts/content-testimonials', get_post_type() );

								endwhile;
								?>
                        <div class="pagination">
                            <a style="opacity:0"
                                href="<?php echo get_post_type_archive_link('testimonials'); ?>?page=<?php echo ++$paged;?>">Next</a>
                        </div>
                        <?php
							else :

								get_template_part( 'template-parts/content', 'none' );

							endif;
							wp_reset_query();
							?>
                    </div>

                </div>
                <a class="pull-right archive-link testimonialsArchiveLink" href="<?php echo home_url(); ?>">
                    <img class="arrow-revert"
                        src="<?php echo get_template_directory_uri() ?>/dist/images/arrow-right.png" alt="">
                </a>
            </div>
        </div>
    </div>
    </div>
</main><!-- #main -->

<?php
get_footer();
?>

<script type="text/javascript">
$(document).ready(function() {
    $('.inf-scroll').initInfScroll();
});
</script>