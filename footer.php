<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package shkenda
 */

?>

<!-- BEGIN FOOTER -->
<div class="lang-popup" id="lang-popup" style="display:none;">
    <div class="logo-area">
        <img src="<?php echo get_template_directory_uri(). '/dist/images/popup-logo.svg'; ?>">
        <h3>Live healthy</h3>
    </div>
    <div class="select-area">
        <!-- <select name="" id="lang-popup-select">
                <option value="" selected>Shteti/Country/Staat</option>
                <option value="sq" data-url="<?php //the_field('sq', 'options'); ?>">Kosova</option>
                <option value="en" data-url="<?php //the_field('en', 'options'); ?>">English</option>
                <option value="de" data-url="<?php //the_field('de', 'options'); ?>">Deutchland</option>
            </select> -->
        <div class="container">
            <div class="lang-popup-select-wrap">
                <ul id="lang-popup-select">
                    <li> <a href="javascript:;" class="lang-popup-select"
                            data-url="<?php the_field('sq', 'options'); ?>">Kosova</a> </li>
                    <li> <a href="javascript:;" class="lang-popup-select"
                            data-url="<?php the_field('en', 'options'); ?>">English</a> </li>
                    <li> <a href="javascript:;" class="lang-popup-select"
                            data-url="<?php the_field('de', 'options'); ?>">deutschland</a> </li>
                </ul>
            </div>
        </div>
    </div>
</div>
<footer id="footer-section" class="main_footer">
    <div class="footerWrap">
        <div class="container has_gutter">
            <div class="row">
                <div class="column-4 column-mob-12">
                    <div class="footer-social-media">
                        <h3><?php the_field('footer_label', 'option'); ?></h3>
                        <ul>
                            <li><a href="<?php the_field('facebook_url', 'option'); ?>" target="_blank"><img
                                        src="<?php echo get_template_directory_uri(). '/dist/images/facebook.svg'; ?>"
                                        alt=""></a></li>
                            <li><a href="<?php the_field('instagram_url', 'option'); ?>" target="_blank"><img
                                        src="<?php echo get_template_directory_uri(). '/dist/images/instagram.svg'; ?>"
                                        alt=""></a></li>
                            <!-- <li><a href="<?php //the_field('youtube_url', 'option'); ?>" target="_blank"><img src="<?php //echo get_template_directory_uri(). '/dist/images/youtube.png'; ?>" alt=""></a></li> -->
                        </ul>
                    </div>
                </div>
                <div class="column-4 column-mob-12">
                    <?php 
                            // Check rows exists.
                            if( have_rows('contact', 'option') ):

                                // Loop through rows.
                                while( have_rows('contact', 'option') ) : the_row();
                        ?>
                    <div class="locations">
                        <h4><?php the_sub_field('country'); ?></h4>
                        <p><?php the_sub_field('address'); ?></p>
                        <a class="google-map-link" style="display: inline-block;" target="_blank"
                            href="<?php the_sub_field('google_map') ?>">Google map</a>
                    </div>
                    <?php  endwhile; endif;?>
                </div>
                <div class="column-4 column-mob-12">
                    <div class="insta-area">
                        <h4>Instagram</h4>
                        <?php  echo do_shortcode("[instagram-feed]"); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>
<!-- END FOOTER -->

<?php wp_footer(); ?>
<script>
function getCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
    }
    return null;
}

function createCookie(name, value, days) {
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        var expires = "; expires=" + date.toGMTString();
    } else var expires = "";
    document.cookie = name + "=" + value + expires + "; path=/";
}

if (getCookie("userLanguage")) {
    if (getCookie("userLanguage") != window.location.origin) {
        window.location = getCookie("userLanguage");
    }
} else {
    $('#lang-popup').css('display', 'flex');

    $('.lang-popup-select').on('click', function(el) {
        createCookie(
            "userLanguage",
            $(this).data('url'),
            30
        );
        window.location = $(this).data('url');
    })
}
</script>
</body>

</html>