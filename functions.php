<?php
/**
 * shkenda functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package shkenda
 */

if ( ! defined( '_S_VERSION' ) ) {
	// Replace the version number of the theme on each release.
	define( '_S_VERSION', '1.0.1' );
}

if ( ! function_exists( 'shkenda_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function shkenda_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on shkenda, use a find and replace
		 * to change 'shkenda' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'shkenda', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus(
			array(
				'menu-1' => esc_html__( 'Primary', 'shkenda' ),
			)
		);

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support(
			'html5',
			array(
				'search-form',
				'comment-form',
				'comment-list',
				'gallery',
				'caption',
				'style',
				'script',
			)
		);

		// Set up the WordPress core custom background feature.
		add_theme_support(
			'custom-background',
			apply_filters(
				'shkenda_custom_background_args',
				array(
					'default-color' => 'ffffff',
					'default-image' => '',
				)
			)
		);

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 *
		 * @link https://codex.wordpress.org/Theme_Logo
		 */
		add_theme_support(
			'custom-logo',
			array(
				'height'      => 250,
				'width'       => 250,
				'flex-width'  => true,
				'flex-height' => true,
			)
		);
	}
endif;
add_action( 'after_setup_theme', 'shkenda_setup' );

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function shkenda_content_width() {
	$GLOBALS['content_width'] = apply_filters( 'shkenda_content_width', 640 );
}
add_action( 'after_setup_theme', 'shkenda_content_width', 0 );

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function shkenda_widgets_init() {
	register_sidebar(
		array(
			'name'          => esc_html__( 'Sidebar', 'shkenda' ),
			'id'            => 'sidebar-1',
			'description'   => esc_html__( 'Add widgets here.', 'shkenda' ),
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);
}
add_action( 'widgets_init', 'shkenda_widgets_init' );

/**
 * Enqueue scripts and styles.
 */
function shkenda_scripts() {
	// wp_style_add_data( 'shkenda-style', 'rtl', 'replace' );
	wp_enqueue_style( 'app-style', get_template_directory_uri(). '/dist/css/app.min.css', array(), _S_VERSION );
	wp_enqueue_style( 'shkenda-style', get_stylesheet_uri(), array(), _S_VERSION );

	// wp_enqueue_script( 'shkenda-navigation', get_template_directory_uri() . '/js/navigation.js', array(), _S_VERSION, true );
	wp_enqueue_script( 'app-js', get_template_directory_uri() . '/dist/js/app.min.js', array(), _S_VERSION, true );
	wp_enqueue_script( 'infinite-scroll', get_template_directory_uri() . '/js/infinite_scroll.min.js', array(), _S_VERSION, true );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'shkenda_scripts' );

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if ( defined( 'JETPACK__VERSION' ) ) {
	require get_template_directory() . '/inc/jetpack.php';
}

if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page(array(
		'page_title' 	=> 'All Labels',
		'menu_title'	=> 'All Labels',
		'menu_slug' 	=> 'all-labels',
	));
	
	acf_add_options_page(array(
		'page_title' 	=> 'Contact Settings',
		'menu_title'	=> 'Contact Settings',
		'menu_slug'	=> 'contact-settings',
	));
	
	acf_add_options_page(array(
		'page_title' 	=> 'Social Media Settings',
		'menu_title'	=> 'Social Media Settings',
		'menu_slug'	=> 'social-media-settings',
	));
	
}

// After registration, logout the user and redirect to home page
function custom_registration_redirect() {
    return home_url('my-account');
}
add_action('woocommerce_registration_redirect', 'custom_registration_redirect', 2);


/**
 * @snippet       WooCommerce User Login Shortcode
 * @how-to        Get CustomizeWoo.com FREE
 * @author        Rodolfo Melogli
 * @compatible    WooCommerce 4.0
 * @donate $9     https://businessbloomer.com/bloomer-armada/
 */
  
add_shortcode( 'wc_login_form', 'login_form' );
  
function login_form() {
   if ( is_admin() ) return;
   if ( is_user_logged_in() ) return; 
   ob_start();
   woocommerce_login_form( array( 'redirect' => home_url() ) );
   return ob_get_clean();
}

function menu_attribute ($atts, $item, $args) {
	//develop your anchor tag ID nomenclature I'll call it $id
	$atts['class'] = "scrollTo";
	$atts['id'] = get_field('id', $item);
	return $atts;
}
add_filter('nav_menu_link_attributes', 'menu_attribute', 10, 3);

//Limit
function limit($value, $limit = 100, $end = '[...]')
{
	if (mb_strlen($value) <= $limit) return $value;
	return mb_substr($value, 0, $limit) . $end;
}


function buy_now_submit_form() {
 ?>
<script>
jQuery(document).ready(function() {
    // listen if someone clicks 'Buy Now' button
    jQuery('#buy_now_button').click(function() {
        // set value to 1
        jQuery('#is_buy_now').val('1');
        //submit the form
        jQuery('form.cart').submit();
    });
});
</script>
<?php
}
add_action('woocommerce_after_add_to_cart_form', 'buy_now_submit_form');

add_filter('woocommerce_add_to_cart_redirect', 'redirect_to_checkout');
function redirect_to_checkout($redirect_url) {
  if (isset($_REQUEST['is_buy_now']) && $_REQUEST['is_buy_now']) {
     global $woocommerce;
     $redirect_url = wc_get_checkout_url();
  }
  return $redirect_url;
}

add_filter( 'woocommerce_checkout_fields' , 'custom_override_checkout_fields' );

function custom_override_checkout_fields( $fields ) {
    unset($fields['billing']['billing_company']);
    unset($fields['billing']['billing_address_2']);
    unset($fields['billing']['billing_postcode']);
    unset($fields['billing']['billing_state']);
    return $fields;
}

add_action( 'admin_post_add_manual_order', 'add_manual_order');
session_start();
 function add_manual_order() {
	$_SESSION['errors'] = [];
	if(!isset($_POST['store'])){
		$_SESSION['errors'][] = 'Store Field should not be empty';
	}
	
	if(!isset($_POST['first_name']) || $_POST['first_name'] == ''){
		$_SESSION['errors'][] = 'First Name Field should not be empty';
	}
	
	if(!isset($_POST['last_name']) || $_POST['last_name'] == ''){
		$_SESSION['errors'][] = 'Last Name Field should not be empty';
	}
	
	if(!isset($_POST['email']) || $_POST['email'] == ''){
		$_SESSION['errors'][] = 'Email Field should not be empty';
	}
	
	if(!isset($_POST['phone'])  || $_POST['phone'] == ''){
		$_SESSION['errors'][] = 'phone Field should not be empty';
	}
	
	if(!isset($_POST['payment']) || $_POST['payment'] == ''){
		$_SESSION['errors'][] = 'Select a payment';
	}
	
	if(in_array($_POST['payment'],['remaining', 'prepaid']) && !isset($_POST['customer_paid'])){
		$_SESSION['errors'][] = 'Add price to payment';
	}
	
	if(!isset($_POST['sessions'])){
		$_SESSION['errors'][] = 'Select sessions';
	}

	if( count($_SESSION['errors']) > 0 ){
		wp_redirect(home_url('/add-order'));
	}

	$address = array(
		'first_name' => $_POST['first_name'],
		'last_name'  => $_POST['last_name'],
		'company'    => $_POST['first_name'],
		'email'      => $_POST['email'],
		'phone'      => $_POST['phone'],
		'address_1'  => '',
		'city'       => '',
		'state'      => '',
		'postcode'   => '',
		'country'    => ''
	);
  
	// Now we create the order
	$order = wc_create_order();
	$sessions = $_POST['volume'] - 1;
	$slug =  'seanca-'.$sessions ?? 'seanca-1';

	$args = array(
	'name'        => $slug,
	'post_type'   => 'product',
	'numberposts' => 1
	);
	$my_posts = get_posts($args);

	$product_obj = new WC_Product( $my_posts[0]->ID);

	// The add_product() function below is located in /plugins/woocommerce/includes/abstracts/abstract_wc_order.php
	$order->add_product(  $product_obj, 1); // This is an existing SIMPLE product
	$order->set_address( $address, 'billing' );

	$order->calculate_totals();
	$order->update_status("Completed", 'Imported order', TRUE);  

	update_field('payment',  $_POST['payment'], $order->ID);
	update_field('store',  $_POST['store'], $order->ID);
	update_field('message',  $_POST['message'], $order->ID);
	update_field('customer_paid',  $_POST['customer_paid'], $order->ID);

	$_SESSION['success'] = 'Porosia eshte shtuar me sukses';
	wp_redirect(home_url('/add-order'));
}