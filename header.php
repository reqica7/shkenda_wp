<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package shkenda
 */

?>
<!doctype html>
<html lang="sq">

<head>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="profile" href="https://gmpg.org/xfn/11">
    <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Playfair+Display:ital,wght@1,900&display=swap"
        rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Martel:wght@200;300;400;600;700;800;900&display=swap"
        rel="stylesheet">
    <?php wp_head(); ?>
    <style>
    .item-thumb-frame {
        position: relative;
        padding-bottom: 56.25%;
        overflow: hidden;
        max-width: 100%;
        height: auto;
    }

    .item-thumb-frame iframe,
    .item-thumb-frame object,
    .item-thumb-frame embed {
        position: absolute;
        top: 0;
        left: 0;
        width: 100% !important;
        height: 100% !important;
    }
    </style>
</head>

<body <?php body_class(); ?>>
    <?php wp_body_open(); ?>
    <header id="header" class="main_header">
        <div class="headerWrap">
            <div class="container">
                <div class="logo">
                    <a href="<?php echo home_url(); ?>">
                        <?php if(wp_is_mobile()): ?>
                        <img src="<?php echo get_template_directory_uri(). '/dist/images/logo-mobile.svg'; ?>">
                        <?php else: ?>
                        <img src="<?php echo get_template_directory_uri(). '/dist/images/logo.svg'; ?>">
                        <?php endif ?>
                    </a>
                </div>
                <div class="mobile-header desktop-hidden">
                    <a href="<?php echo wc_get_cart_url(); ?>">
                        <img src="<?php echo get_template_directory_uri(). '/dist/images/cart.svg'; ?>" alt="">
                    </a>
                    <a href="<?php echo get_permalink( wc_get_page_id( 'myaccount' ) ); ?>">
                        <img src="<?php echo get_template_directory_uri(). '/dist/images/user.svg'; ?>" alt="">
                    </a>
                </div>
                <div class="hamburger">
                    <span></span>
                    <span></span>
                    <span></span>
                </div>
                <div class="main-nav">
                    <?php
                wp_nav_menu(
                  array(
                    'theme_location' => 'menu-1',
                    'container_class' => 'main-menu',
                    'menu_class' => '',
                    'menu_id' => 'main-menu',
                    )
                  );
                ?>
                    <div class="right-side-header__social-media desktop-hidden">
                        <ul>
                            <li><a href="<?php the_field('facebook_url', 'option'); ?>" target="_blank"><img
                                        src="<?php echo get_template_directory_uri(). '/dist/images/facebook.svg'; ?>"
                                        alt=""></a></li>
                            <li><a href="<?php the_field('twitter_url', 'option'); ?>" target="_blank"><img
                                        src="<?php echo get_template_directory_uri(). '/dist/images/twitter.svg'; ?>"
                                        alt=""></a></li>
                            <li><a href="<?php the_field('instagram_url', 'option'); ?>" target="_blank"><img
                                        src="<?php echo get_template_directory_uri(). '/dist/images/instagram.svg'; ?>"
                                        alt=""></a></li>
                        </ul>
                    </div>
                </div>
                <div class="right-side-header desktop-hidden">
                    <div class="right-side-header__cart mobile-hidden">
                        <a href="<?php echo wc_get_cart_url(); ?>">
                            <img src="<?php echo get_template_directory_uri(). '/dist/images/cart.svg'; ?>" alt="">
                        </a>
                    </div>
                    <div class="right-side-header__social-media mobile-hidden">
                        <ul>
                            <li><a href="<?php the_field('facebook_url', 'option'); ?>" target="_blank"><img
                                        src="<?php echo get_template_directory_uri(). '/dist/images/facebook.svg'; ?>"
                                        alt=""></a></li>
                            <li><a href="<?php the_field('twitter_url', 'option'); ?>" target="_blank"><img
                                        src="<?php echo get_template_directory_uri(). '/dist/images/twitter.svg'; ?>"
                                        alt=""></a></li>
                            <li><a href="<?php the_field('instagram_url', 'option'); ?>" target="_blank"><img
                                        src="<?php echo get_template_directory_uri(). '/dist/images/instagram.svg'; ?>"
                                        alt=""></a></li>
                        </ul>
                    </div>
                    <div class="right-side-header__user-area mobile-hidden">
                        <a href="<?php echo get_permalink( wc_get_page_id( 'myaccount' ) ); ?>">
                            <img src="<?php echo get_template_directory_uri(). '/dist/images/user.svg'; ?>" alt="">
                        </a>
                    </div>
                </div>
                <div class="right-side-header mobile-hidden">
                    <div class="right-side-header__cart">
                        <a href="<?php echo wc_get_cart_url(); ?>">
                            <img src="<?php echo get_template_directory_uri(). '/dist/images/cart.svg'; ?>" alt="">
                        </a>
                    </div>
                    <div class="right-side-header__social-media">
                        <ul>
                            <li><a href="<?php the_field('facebook_url', 'option'); ?>" target="_blank"><img
                                        src="<?php echo get_template_directory_uri(). '/dist/images/facebook.svg'; ?>"
                                        alt=""></a></li>
                            <li><a href="<?php the_field('twitter_url', 'option'); ?>" target="_blank"><img
                                        src="<?php echo get_template_directory_uri(). '/dist/images/twitter.svg'; ?>"
                                        alt=""></a></li>
                            <li><a href="<?php the_field('instagram_url', 'option'); ?>" target="_blank"><img
                                        src="<?php echo get_template_directory_uri(). '/dist/images/instagram.svg'; ?>"
                                        alt=""></a></li>
                        </ul>
                    </div>
                    <div class="right-side-header__user-area">
                        <a href="<?php echo get_permalink( wc_get_page_id( 'myaccount' ) ); ?>">
                            <img src="<?php echo get_template_directory_uri(). '/dist/images/user.svg'; ?>" alt="">
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </header>