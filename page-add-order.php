<?php
/**
* Template Name: Add Order
*
* @package WordPress
* @subpackage Shkenda
* @since Shkenda 1.0
*/
session_start();
if ( !(is_user_logged_in() && ( current_user_can('editor') || current_user_can('administrator') )) ) {
    wp_redirect(home_url('/'));
}

get_header();

?>
<div class="woocommerce">
    <div class="container">
        <form method="post" class="woocommerce-form woocommerce-form-register register form-register-holder" action="<?php echo admin_url( 'admin-post.php' ); ?>" >
            <input type="hidden" name="action" value="add_manual_order">
            <input type="hidden" name="add_manual_order_nonce" value="<?php echo wp_create_nonce( 'add_manual_order_nonce' ); ?>" />	
            <div class="col1-set" id="customer_details">
                <div class="col-1">
                    <div class="woocommerce-billing-fields">
                        <h3>Forma regjistrimit</h3>
                        <?php if(isset($_SESSION['errors']) &&  count($_SESSION['errors']) > 0): ?>
                        <?php foreach($_SESSION['errors'] as $error): ?>
                            <p style="color:red"><?php echo $error;?></p>
                        <?php endforeach; endif; ?>
                        <?php if(isset($_SESSION['success']) ): ?>
                            <p style="color:green"><?php echo $_SESSION['success'];?></p>
                        <?php endif; ?>
                        <div class="woocommerce-billing-fields__field-wrapper">
                            <div class="column-6 column-mob-12">
                            <p class="form-row address-field validate-required form-row-wide" id="store_field" data-priority="10">
                                <label for="store" class="">Pika e shitjes<abbr class="required" title="required">*</abbr></label>
                                <span class="woocommerce-input-wrapper">
                                    <input type="text" class="input-text" name="store" id="store" placeholder="">
                                </span>
                            </p>
                            <p class="form-row form-row-first validate-required" id="first_name_field" data-priority="10">
                                <label for="first_name" class="">Emri&nbsp;<abbr class="required" title="required">*</abbr></label>
                                <span class="woocommerce-input-wrapper">
                                    <input type="text" class="input-text " name="first_name" id="first_name" placeholder="">
                                </span>
                            </p>
                            <p class="form-row form-row-last validate-required" id="last_name_field" data-priority="20">
                                <label for="last_name" class="">Last name&nbsp;<abbr class="required" title="required">*</abbr></label>
                                <span class="woocommerce-input-wrapper">
                                    <input type="text" class="input-text " name="last_name" id="last_name" placeholder="">
                                </span>
                            </p>
                           
                            <p class="form-row address-field validate-required form-row-wide" id="email_field" data-priority="10">
                                <label for="email" class="">Email<abbr class="required" title="required">*</abbr></label>
                                <span class="woocommerce-input-wrapper">
                                    <input type="email" class="input-text" name="email" id="email" placeholder="">
                                </span>
                            </p>
                            <p class="form-row address-field validate-required form-row-wide" id="phone_field" data-priority="10">
                                <label for="phone" class="">Telefoni<abbr class="required" title="required">*</abbr></label>
                                <span class="woocommerce-input-wrapper">
                                    <input type="text" class="input-text" name="phone" id="phone" placeholder="">
                                </span>
                            </p>
                            <p class="form-row address-field validate-required form-row-wide" id="sessions_field" data-priority="10">
                                <label for="sessions" class="">Numri i seancave<abbr class="required" title="required">*</abbr></label>
                                <span class="woocommerce-input-wrapper">
                                <input type="range" id="sessions" name="sessions" min="0" step="5" max="20">
                                </span>
                            </p>

                            

                            <p class="form-row address-field validate-required form-row-wide" id="payment_field" data-priority="10">
                                <label for="payment" class="">Lloji i pagesave<abbr class="required" title="required">*</abbr></label>
                                <span class="woocommerce-input-wrapper">
                                <input type="radio" id="prepaid" name="payment" value="prepaid">
                                    <label for="prepaid">Parapagim</label>
                                <input type="radio" id="remaining" name="payment" value="remaining">
                                    <label for="remaining">Pagesa e mbetur</label>
                                <input type="radio" id="paid" name="payment" value="paid">
                                    <label for="paid">Pagesa e kompletuar</label>
                                </span>
                            </p>

                            <p class="form-row address-field validate-required form-row-wide" id="payment_field" data-priority="10">
                                <input type="number" name="customer_paid">
                            </p>

                            <button type="submit" class="button alt green-btn " id="place_order" value="Place order">Place order</button>
                        </div>
                            <div class="column-6 column-mob-12 pd-100">

                                <p class="form-row form-row-last validate-required " id="message_field" data-priority="20">
                                    <label for="last_name" class="">Informacion shtesë</label>
                                    <span class="woocommerce-input-wrapper">
                                        <label for="">Shënime porosie (opsionale)</label>
                                        <textarea name="message" cols="30" rows="10"></textarea>
                                    </span>
                                </p>
                                <p class="column-12">Të gjitha hapësirat e shënuara me * duhet të plotësohen patjetër.</p>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </form>
    </div>
</div>
<?php

get_footer();
