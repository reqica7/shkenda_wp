<?php
/**
* Template Name: Auth Forms
*
* @package WordPress
* @subpackage Shkenda
* @since Shkenda 1.0
*/

if ( is_admin() || is_user_logged_in() ) {
    wp_redirect(home_url('my-account'));
}

get_header();

do_action( 'woocommerce_before_customer_login_form' );
 
?>
<div class="fullBlock leaf-background">
<div class="form-holder">
    <form method="post" class="woocommerce-form woocommerce-form-register register" <?php do_action( 'woocommerce_register_form_tag' ); ?> >
        <div class="form-head">
            <h3>Hello, friend</h3>
        </div>
        <?php do_action( 'woocommerce_register_form_start' ); ?>

        <p class="form-row form-row-first">
            <input type="text" class="input-text woocommerce-Input name" name="billing_first_name" id="reg_billing_first_name" value="<?php if ( ! empty( $_POST['billing_first_name'] ) ) esc_attr_e( $_POST['billing_first_name'] ); ?>" />
        </p>

        <p class="form-row form-row-last">
            <input type="text" class="input-text woocommerce-Input name" name="billing_last_name" id="reg_billing_last_name" value="<?php if ( ! empty( $_POST['billing_last_name'] ) ) esc_attr_e( $_POST['billing_last_name'] ); ?>" />
        </p>

        <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
            <input type="email" class="woocommerce-Input woocommerce-Input--text input-text" name="email" id="username" autocomplete="email" value="<?php echo ( ! empty( $_POST['email'] ) ) ? esc_attr( wp_unslash( $_POST['email'] ) ) : ''; ?>" /><?php // @codingStandardsIgnoreLine ?>
        </p>

        <?php if ( 'no' === get_option( 'woocommerce_registration_generate_password' ) ) : ?>

            <p class="woocommerce-form-row woocommerce-form-row--wide form-row form-row-wide">
                <input type="password" class="woocommerce-Input woocommerce-Input--text input-text" name="password" id="password" autocomplete="new-password" />
            </p>

        <?php else : ?>

            <p><?php esc_html_e( 'A password will be sent to your email address.', 'woocommerce' ); ?></p>

        <?php endif; ?>


        <p class="woocommerce-FormRow form-row">
            <?php wp_nonce_field( 'woocommerce-register', 'woocommerce-register-nonce' ); ?>
            <button type="submit" class="woocommerce-Button woocommerce-button button woocommerce-form-register__submit" name="register" value="<?php esc_attr_e( 'Register', 'woocommerce' ); ?>"><?php esc_html_e( 'Register', 'woocommerce' ); ?></button>
        </p>

        <?php do_action( 'woocommerce_register_form_end' ); ?>

    </form>
    <div class="form-dsc">
        <h2>Glad to see you</h2>
        <p>Lorem ipsum dolor ahmet ipsum dolor ahmet ipsum dolor ahmet ipsum dolorahmet ipsum dolor </p>
    </div>
   </div>
   </div>
<?php

get_footer();
