<?php
/**
* Template Name: Homepage
*
* @package WordPress
* @subpackage Shkenda
* @since Shkenda 1.0
*/
get_header();
?>

<!-- BEGIN MAIN -->
<main id="main" class="main_content">
    <div class="mainWrap">
        <div class="mainSlider" id="home-section">
            <div class="owl-carousel owl-theme main-slider">
                <?php 
                        // Check rows exists.
                        if( have_rows('banner') ):

                            // Loop through rows.
                            while( have_rows('banner') ) : the_row();
                    ?>
                <div class="item">
                    <div class="item-content">
                        <div class="item-content-img">
                            <img src="<?php echo get_sub_field('image'); ?>" alt="">
                            <div class="dsc">
                                <h1><?php echo get_sub_field('title'); ?></h1>
                                <?php echo apply_filters('the_content',  wp_trim_words( strip_tags(   get_sub_field('description') ), 25, "..." )); ?>
                                <a href="<?php echo get_sub_field('button_url'); ?>"
                                    class="green-btn"><?php echo get_sub_field('button_title'); ?></a>
                            </div>
                        </div>
                    </div>
                </div>
                <?php
                            // End loop.
                            endwhile;
                        endif;
                    ?>

            </div>
        </div>
        <?php $aboutUs = get_field('about_us');?>
        <div class="fullBlock welcome-section">
            <div class="container">
                <div class="fullBlock">
                    <h5 class="border-text"><?php the_field('welcome_label', 'option'); ?></h5>
                </div>
                <div class="top-services">
                    <h3><?php echo $aboutUs['title'] ?></h3>
                    <ul>
                        <?php 
                            // Check rows exists.
                            if($aboutUs['icons'] ):

                                // Loop through rows.
                                foreach($aboutUs['icons'] as $icon ):
                        ?>
                        <li>
                            <img src="<?php echo $icon['icon']; ?>" alt="">
                            <p><?php echo $icon['title']; ?></p>
                        </li>
                        <?php 
                            endforeach;
                            endif;
                        ?>
                    </ul>
                </div>
                <div class="services-slider-holder" id="aboutus-section">
                    <div class="column-10 column-mob-12 slider-dsc">
                        <?php echo $aboutUs['description']; ?>
                    </div>
                    <div class="owl-carousel owl-theme services-slider">
                        <?php 
                                // Check rows exists.
                                if($aboutUs['gallery'] ):

                                    // Loop through rows.
                                    foreach($aboutUs['gallery'] as $galleryItem ):
                            ?>
                        <div class="item">

                            <?php if($galleryItem['is_image']): ?>
                            <div class="item-thumb">
                                <img src="<?php echo $galleryItem['image']; ?>" alt="">
                            </div>
                            <?php else : ?>
                            <div class="item-thumb-frame">
                                <?php echo $galleryItem['video']; ?>
                            </div>
                            <?php endif; ?>
                            <div class="slider-dsc">
                                <?php echo apply_filters('the_content', wp_trim_words( strip_tags(  $galleryItem['description'] ), 55, "..." )); ?>
                            </div>

                        </div>
                        <?php 
                                endforeach;
                                endif;
                            ?>
                    </div>
                </div>
            </div>
        </div>
        <?php 
            $trainers = get_field("trainers");
            $trainerPosts = $trainers['trainers'];
            if( $trainerPosts ):
        ?>
        <div class="fullBlock" id="trainers-section">
            <div class="trainer-section">
                <div class="container has_gutter">
                    <div class="row">
                        <div class="column-3 column-tab-12 column-mob-12 trainer-item first-item">
                            <h4><?php echo $trainers['title']; ?></h4>
                            <?php echo apply_filters('the_content', $trainers['description']); ?>
                        </div>
                        <?php 
                                foreach( $trainerPosts as $trainerPost ): 
                        ?>
                        <div class="column-3 column-tab-4 column-mob-12 trainer-item">
                            <a href="<?php the_permalink($trainerPost->ID); ?>">
                                <div class="thumb">
                                    <img src="<?php echo get_the_post_thumbnail_url($trainerPost->ID); ?>" alt="">
                                </div>
                                <div class="dsc">
                                    <h5><?php echo get_the_title($trainerPost->ID); ?></h5>
                                    <?php  //echo apply_filters('the_content', $trainerPost->post_content); ?>
                                    <p> <?php echo wp_strip_all_tags(limit($trainerPost->post_content,360)); ?> </p>
                                </div>
                            </a>
                        </div>
                        <?php 
                                endforeach;
                        ?>
                    </div>
                </div>
            </div>
        </div>
        <?php endif; ?>
        <div class="fullBlock" id="products-section">
            <div class="products-section">
                <div class="container has_gutter">
                    <h2><?php the_field('product_label', 'option'); ?></h2>
                    <div class="row">
                        <?php $productPosts = get_field("products"); 
                                if( $productPosts ): 
                                    foreach( $productPosts as $productPost ): 
                            ?>
                        <div class="column-2-5 column-tab-6 column-mob-12">
                            <div class="product-item">
                                <a href="<?php the_permalink($productPost->ID); ?>">
                                    <div class="product-item-thumb">
                                        <img src="<?php echo get_the_post_thumbnail_url($productPost->ID); ?>" alt="">
                                    </div>
                                    <div class="product-item-dsc">
                                        <h5><?php echo get_the_title($productPost->ID); ?></h5>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <?php 
                                    endforeach;
                                endif;
                            ?>
                    </div>
                    <div class="offer-list" id="pakot-section">
                        <div class="row">
                            <?php $packages = get_field("package"); ?>
                            <div class="column-3 column-mob-12 offer-list-item">
                                <h4><?php the_field('package_label', 'option'); ?></h4>
                                <h3><?php echo $packages['title']; ?></h3>
                                <?php echo apply_filters('the_content', $packages['description']); ?>
                            </div>
                            <?php
                                    if( $packages["packages"] ): 
                                        foreach( $packages["packages"] as $package ): 
                                            $product = wc_get_product( $package->ID );
                                            $hasSalePrice = !!$product->sale_price;
                                ?>
                            <div class="column-3 column-tab-4 column-mob-12 offer-list-item">
                                <div class="offer-list-item-inner">
                                    <?php if($hasSalePrice) : ?>

                                    <span
                                        class="sale-badge"><?php echo round( ( ( $product->regular_price - $product->sale_price ) / $product->regular_price ) * 100 );  ?>%</span>

                                    <?php endif; ?>
                                    <div class="offer-list-area">
                                        <h5 class="offer-title"><?php echo $package->post_title;?></h5>
                                        <span class="sessions"><?php echo  get_field("sessions", $package->ID);?></span>
                                        <p><?php echo $hasSalePrice ? $product->sale_price : $product->regular_price; ?>
                                            <?php echo get_option('woocommerce_currency'); ?> </p>
                                        <ul>
                                            <?php $productDetails = get_field("package_details", $package->ID);
                                                    if(  $productDetails ): 
                                                        foreach( $productDetails as $detail ):  ?>
                                            <li><?php echo $detail['name'];?></li>
                                            <?php endforeach; endif; ?>
                                        </ul>
                                        <a class="buy-now"
                                            href="<?php echo $product->get_permalink(); ?>"><?php the_field('buy_label', 'option'); ?></a>
                                    </div>
                                </div>
                            </div>
                            <?php endforeach; endif; ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php $testimonials = get_field("testimonials"); ?>
        <div class="fullBlock">
            <div class="testimonials" id="testimonials-section">
                <div class="container">
                    <div class="row has_gutter">

                        <?php
                    if( $testimonials["testimonials"] ): 
                        foreach( $testimonials["testimonials"] as $testimonial ): 
                ?>
                        <div class="column-4 column-mob-12">
                            <div class="testimonials__item">
                                <div class="testimonials__item__img">
                                    <img src="<?php echo get_the_post_thumbnail_url($testimonial->ID); ?>" alt="">
                                </div>
                                <div class="testimonials__item__dsc">
                                    <h5><?php echo $testimonial->post_title; ?></h5>
                                    <span><?php echo get_field("sessions_number", $testimonial->ID); ?></span>
                                    <span><b><?php echo get_field("lost_weight", $testimonial->ID); ?></b></span>
                                </div>
                            </div>
                        </div>
                        <?php 
                        endforeach;
                    endif;
                ?>
                        <a class="pull-right archive-link"
                            href="<?php echo get_post_type_archive_link('testimonials'); ?>">
                            <img src="<?php echo get_template_directory_uri() ?>/dist/images/arrow-right.png" alt="">
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
<!-- END MAIN -->

<?php
get_footer();
?>
<script>
$(document).ready(function() {
    const urlHash = window.location.href.split("#");
    $('#main-menu').find('li').not(':first').removeClass('current-menu-item');

    if (urlHash.length > 1) {
        $('#main-menu').find('li').removeClass('current-menu-item');
        $('#main-menu').find('#' + urlHash[1]).parent().addClass('current-menu-item');

        var scrollTo = $('#' + urlHash[1] + '-section').offset().top - 150;

        $("html,body").animate({
                scrollTop: scrollTo,
            },
            500
        );
    }

    $('#main-menu > li > a').on('click', function(e) {
        $('#main-menu').find('li').removeClass('current-menu-item');
        e.target.parentElement.classList.toggle('current-menu-item')
    })

    $(".scrollTo").click(function(e) {
        var id = $(this).attr("href").split("#");

        if (id.length > 1) {
            e.preventDefault();

            var scrollTo = $('#' + id[1] + '-section').offset().top - 150;

            $("html,body").animate({
                    scrollTop: scrollTo,
                },
                500
            );
        }
    });
});
</script>