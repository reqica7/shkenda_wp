<?php
/**
 * The template for displaying all single posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/#single-post
 *
 * @package shkenda
 */

get_header();
?>
	<div class="single-trainers-wrapper">
		<div class="container has_gutter">
			<div class="row">
				<?php
					while ( have_posts() ) :
						the_post();
						get_template_part( 'template-parts/content', 'products' );
					endwhile; // End of the loop.
				?>
			</div>
		</div>
	</div>

<?php
get_footer();
