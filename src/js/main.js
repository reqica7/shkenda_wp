(function ($, window, document) {
  "use strict";
  $(function () {
    //##Variables
    var $body = $("body"),
      $window = $(window),
      $doc = $(document),
      defaultEasing = [0.4, 0.0, 0.2, 1];
    //End Variables

    $(".main-slider").owlCarousel({
      loop: true,
      margin: 0,
      dots: false,
      nav: true,
      responsive: {
        0: {
          items: 1,
        },
        600: {
          items: 1,
        },
        1000: {
          items: 1,
        },
      },
    });
    $(".services-slider").owlCarousel({
      loop: true,
      margin: 0,
      nav: false,
      dots: true,
      responsive: {
        0: {
          items: 1,
        },
        600: {
          items: 1,
        },
        1000: {
          items: 1,
        },
      },
    });
    $(".hamburger").on("click", function (e) {
      $(".main-nav").toggleClass("menutoggled");

      $(this).toggleClass("toggled");
      $(".main-nav").slideToggle("fast");
    });
  });
})(window.jQuery, window, document);
