<?php
/**
 * Template part for displaying page content in page.php
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package shkenda
 */

?>
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
    <div class="column-12">
        <?php if(has_post_thumbnail()): ?>
        <div class="single-thumbnail">
            <?php echo the_post_thumbnail('full'); ?>
        </div>
        <?php endif ?>
        <div class="dsc">
            <h3><?php the_title(); ?></h3>
        </div>
    </div>
    <div class="column-12">
        <div class="single-content" style="padding-left: 0;">
            <?php the_content(); ?>
        </div>
    </div>
</article><!-- #post-<?php the_ID(); ?> -->