<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package shkenda
 */

?>
<div class="column-12">
	
	<?php the_content(); ?>
</div>
