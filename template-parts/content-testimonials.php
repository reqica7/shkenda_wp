<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package shkenda
 */

?>

<div class="column-4 column-mob-12 testimonialsList">
    <div class="testimonials__item">
        <div class="testimonials__item__img">
            <?php the_post_thumbnail('full'); ?>
        </div>
        <div class="testimonials__item__dsc">
            <h5><?php the_title(); ?></h5>
            <span><?php echo get_field("sessions_number", get_the_id()); ?></span>
            <span><b><?php echo get_field("lost_weight", get_the_id()); ?></b></span>
        </div>
    </div>
</div>