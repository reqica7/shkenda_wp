<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package shkenda
 */

?>
<div class="column-6 column-mob-12">
	<div class="single-thumbnail">
		<?php echo the_post_thumbnail('full'); ?>
	</div>
</div>
<div class="column-6 column-mob-12">
	<div class="single-content">
		<h3><?php the_title(); ?></h3>
		<?php the_content(); ?>
		<!-- <div class="right-side-header__social-media">
			<ul>
				<li><a href="https://www.facebook.com/qendraedobesimit" target="_blank"><img src="http://localhost/2021/shkendadubova/wp-content/themes/shkenda/dist/images/facebook.svg" alt=""></a></li>
				<li><a href="" target="_blank"><img src="http://localhost/2021/shkendadubova/wp-content/themes/shkenda/dist/images/twitter.svg" alt=""></a></li>
				<li><a href="https://www.instagram.com/qenderdobesimi_shkendadubova/" target="_blank"><img src="http://localhost/2021/shkendadubova/wp-content/themes/shkenda/dist/images/instagram.svg" alt=""></a></li>
			</ul>
		</div> -->
	</div>
</div>
