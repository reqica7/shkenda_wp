<?php
/**
 * Template part for displaying posts
 *
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package shkenda
 */

?>

<div class="column-4 column-mob-12">
	<div class="single-thumbnail">
		<?php echo the_post_thumbnail('full'); ?>
	</div>
	<div class="dsc">
		<h3><?php the_title(); ?></h3>	
	</div>
</div>
<div class="column-8 column-mob-12">
	<div class="single-content">
		<span><?php the_field('session_field', get_the_id()); ?></span>
		<h4><?php the_field('subtitle', get_the_id()); ?></h4>
		<?php the_content(); ?>
	</div>
</div>
