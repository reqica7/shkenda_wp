<?php
/**
 * Thankyou page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/thankyou.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.7.0
 */

defined( 'ABSPATH' ) || exit;
?>

	<div class="fullBlock leaf-background leaf-square">
		<div class="form-holder page-404">
			<div class="u-columns col2-set" id="customer_login">
				<div class="u-column1 col-1">
					<div class="woocommerce-form">
						<div class="form-head">
							
							<?php the_field('thank_you', 'option'); ?>
							<a href="<?php echo home_url(); ?>" class="green-btn returnHome"><?php the_field('return_home', 'option'); ?></a>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>